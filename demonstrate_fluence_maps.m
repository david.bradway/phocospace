%% Demonstrate example fluence profiles and associated SLSC images

clear all
close all 
clc

addpath(genpath('utils'));
f1 = figure('Position',[386 30 1135 934]);
f_CC = figure('Position',[617 329 638 543]);
set(0,'defaultAxesFontSize',12);

subplot_idx_C1 = 1;
subplot_idx_C2 = 2;

f_width          = {inf;
                    25e-3;
                    5e-3;
                    1e-3}; % initial beam width of Gaussian fluence profile [m]
                
f_width_str = {'inf',...
               '25',...
               '5',...
               '1'}; % string indicating initial beam width of Gaussian fluence profiles [mm] 

% xos = [96,21,64];    
% zos = [185,222,128];

target_index = 1; %[7 mm = 1, 5 mm = 5, 2 mm = 3]
target_str = '7';

sv = false; %flag to save data or not

s = cell(numel(f_width),2); %cell to store subplot axes for modification

for i = 1:numel(f_width)
       
    load(fullfile('target_maps','polka_dots.mat'));
    
    target_case = 'normal';
    seed_number = 3;
    
    %% Make transducer
    [transducer,~] = makeTransducer();
    
    %% Make optical properties/absorber distribution
    [mu_map, g_map, chi_map, target_map] = makeOpticalPhantom(target_map,'randomEverywhere',dx,seed_number,...
                                           'TargetCase',target_case,'PlotLayout',false);

    %% Make fluence distribution
    fiber_width = f_width{i}; %5e-3; %[m]
    fiber_depth = round(size(target_map,1)/2) - 12e-3/2/dx;
    [f_map] = makeFluence(target_map,fiber_width,dx,...
                          'FiberDepth',fiber_depth,...
                          'Attenuation',true,...
                          'PlotLayout',false);

    figure(f1);
    subplot(4,2,subplot_idx_C1);
    imagesc([0:dx:w].*1e3,[0:dx:z].*1e3,f_map,[0 0.005]);%colormap jet;
    ylabel('mm');%xlabel('(mm)');
    axis image
    colorbar;
    
    ax = gca;
    s{i,1} = ax;                                            
    
    %% Make pressure map
    snr = 10; %25;    % desired SNR of initial pressure distribution [dB] (note snr = nan means no noise is added)
    [~,pMap_smooth,~] = makePressure(target_map, chi_map, mu_map, g_map, f_map,...
                                     snr, seed_number, dx,...
                                     'ZeroMean', true, 'PlotLayout', false);

    %% Run theoretical SLSC algorithm
    sprintf('Calculating SLSC')

    % =========================================================================
    % SIMULATION
    % =========================================================================
    maxM = 30;
    tic
    [slsc_theory] = makeSLSC(pMap_smooth, target_map, transducer,dx,target_case,...
                             'SimRange', 'image',...
                             'maxM',maxM,...
                             'DepthOfInterest',zos(target_index));
    toc
    
    %
    xos = xos(target_index);
    zos = zos(target_index);
    
    if sv
        save(fullfile('phocoSpace_figs',sprintf('slsc_theory_polkadot3_%smm_%smmfwidth.mat',target_str,f_width_str{i})),...
                                        'slsc_theory','xos','zos','transducer');
    end
   
    %% Plotting
    M = 14;

    figure(f1);
    subplot(4,2,subplot_idx_C2);
    imagesc([0:dx:w].*1e3,[0:dx:z].*1e3,slsc_theory.slsc_avg(:,:,10)./max(max(slsc_theory.slsc_avg(:,:,10))),[0 1]);colormap gray;
    ylabel('mm');%xlabel('(mm)');
    axis image
    colorbar;
    
    ax = gca;
    s{i,2} = ax; 
           
    % update plotting indexes
    subplot_idx_C1 = subplot_idx_C1 +2;
    subplot_idx_C2 = subplot_idx_C2 +2;
end

% Adjust axes
pause(0.01);
s{1,2}.Position = [s{1,2}.Position(1) - 0.18, s{1,2}.Position(2) + 0.025*0, s{1,2}.Position(3),s{1,2}.Position(4)];
s{2,2}.Position = [s{2,2}.Position(1) - 0.18, s{2,2}.Position(2) + 0.025*1, s{2,2}.Position(3),s{2,2}.Position(4)];
s{3,2}.Position = [s{3,2}.Position(1) - 0.18, s{3,2}.Position(2) + 0.025*2, s{3,2}.Position(3),s{3,2}.Position(4)];
s{4,2}.Position = [s{4,2}.Position(1) - 0.18, s{4,2}.Position(2) + 0.025*3, s{4,2}.Position(3),s{4,2}.Position(4)];
s{2,1}.Position = [s{2,1}.Position(1) , s{2,1}.Position(2) + 0.025*1, s{2,1}.Position(3),s{2,1}.Position(4)];
s{3,1}.Position = [s{3,1}.Position(1) , s{3,1}.Position(2) + 0.025*2, s{3,1}.Position(3),s{3,1}.Position(4)];
s{4,1}.Position = [s{4,1}.Position(1) , s{4,1}.Position(2) + 0.025*3, s{4,1}.Position(3),s{4,1}.Position(4)];

s{1,2}.YAxis.Visible = 'off';
s{2,2}.YAxis.Visible = 'off';
s{3,2}.YAxis.Visible = 'off';
s{4,2}.YAxis.Visible = 'off';


s{4,1}.XLabel.String = 'mm\newline (g)';  
s{4,2}.XLabel.String = 'mm\newline (h)';  
s{3,2}.XTick = 38.4/2;s{3,2}.XTickLabel = '(f)';
s{3,1}.XTick = 38.4/2;s{3,1}.XTickLabel = '(e)';
s{2,2}.XTick = 38.4/2;s{2,2}.XTickLabel = '(d)';
s{2,1}.XTick = 38.4/2;s{2,1}.XTickLabel = '(c)';
s{1,2}.XTick = 38.4/2;s{1,2}.XTickLabel = '(b)';
s{1,1}.XTick = 38.4/2;s{1,1}.XTickLabel = '(a)';

s{1,1}.Title.String = 'Fluence Profile';
s{1,2}.Title.String = 'Theoretical SLSC Image';



