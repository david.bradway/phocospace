# PhocoSpace: An open-source simulation package to implement photoacoustic spatial coherence theory 

## Description

We previously developed a novel photoacoustic spatial coherence theory and demonstrated its ability to model and optimize a wide range of  photoacoustic short-lag spatial coherence (SLSC) imaging parameters, target geometries, and light profile sizes. Recognizing the broad potential of this theoretical application, we developed __PhocoSpace: an open-source photoacoustic toolbox to simulate the coherence of photoacoustic signals correlated in the transducer space dimension__. PhocoSpace is provided as a MATLAB extension. Target geometries, fluence distributions within an imaging plane, and parameters describing acoustic receivers (e.g., transducer element pitch, bandwidth, aperture size) may each be customized to model experimental setups, clinical scenarios, and imaging equipment specifications. PhocoSpace is a flexible _in silico_ tool to predict photoacoustic spatial coherence functions, determine expected photoacoustic SLSC image quality, and characterize multiple possible coherence-based photoacoustic image optimizations without requiring lengthy experimental data acquisition. In addition, this software package establishes a foundation for future investigations into alternative photoacoustic spatial coherence-based signal processing methods.

If you use this code, please cite the following two references:

1. M.T. Graham and M.A.L. Bell, "PhocoSpace: An open-source simulation package to implement photoaocustic spatial coherence theory," in 2022 IEEE International Ultrasonics Symposium (IUS), IEEE 2022 (accepted). (currently available as a preprint on \[[pdf](https://pulselab.jhu.edu/wp-content/uploads/2022/09/GrahamBell_PhocoSpace_IUS2022.pdf)\]
2. M.T. Graham and M.A.L. Bell, "Photoacoustic spatial coherence theory and applications to coherence-based image contrast and resolution," IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control, vol. 67, no. 10, pp. 2069-2084, 2020. \[[pdf](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9106409&tag=1)\]

## Instructions for Use
Reference \[1\] serves as an introductory user manual containing tutorials to illustrate PhocoSpace's capabilities.

## Copyright
&copy; 2022 Michelle T. Graham and Muyinatu A. Lediju Bell, All rights reserved. 
