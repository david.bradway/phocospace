%% Generates optical absorption and Gruneisen distributions

% INPUTS
%   target_map         --> (matrix) binary map of target location
%   distribution_type --> (string) type of target distribution, options are
%                         'constant', 'randomTarget', 'randomEverywhere'
%   dx                --> (numeric) resolution (symmetric) of imaging field [m]
%   seed_number       --> (numeric) seed for random number generator

% INPUTS (optional)
%   'PlotLayout        --> (logical) choose whether or not to plot target distribution (default == true)
%   'TargetCase'       --> (string) choose if simulation is for a point
%                           target, options are 'point' and 'normal (defult = 'normal')
%   xo                 --> (numeric) target lateral center [gridpoints]
%   zo                 --> (numeric) target axial center   [gridpoints]
%   mu_properties      --> (numeric) 1 x 2 vector of background, target property
%   g_properties       --> (numeric) 1 x 2 vector of background, target property
%   density            --> (numeric) [absorbers/m2]

% OUTPUTS
%   mu                 --> (numeric) optical absorption distribution map
%   g                  --> (numeric) Gruneisen parameter distribution map
%   chi                --> (numeric) absorber distribution map
%   target_map         --> (numeric) binary map of target_map location padded laterally with zeros to avoid edge effects


function [mu, g, chi, target_map] = makeOpticalPhantom(target_map,distribution_type,dx,seed_number,varargin)
    
%% Parse inputs
    p = inputParser;

    addParameter(p,'PlotLayout',true);
    addParameter(p,'TargetCase','normal');
    addParameter(p,'xo',round(size(target_map,2)/2),@isnumeric);
    addParameter(p,'zo',round(size(target_map,1)/2),@isnumeric);  
    addParameter(p,'mu_properties',[0.10,111],@isnumeric);   % [background, target]  [cm^-1]
    addParameter(p,'g_properties' ,[0.81,0.144],@isnumeric); % [background, target]  [dimensionless]
    addParameter(p,'density', 4e7,@isnumeric);               %  density [absorbers/m2]
    
    parse(p,varargin{:})    %parse the inputs
    
    % Extend lateral dimension of targetmap to avoid edge effects of fourier transform
    nz_og  = size(target_map,2);
    nz_ext  = nz_og*1.5;
    
    pd = round(nz_ext - nz_og)/2;
    tmp = zeros(size(target_map,1),pd);
    
    target_map = [tmp, target_map, tmp];
    
    %% Initialize optical absorbance and gruneisen matrices
    mu = zeros(size(target_map));      % optical absorbance matrix
    g = zeros(size(target_map));        % gruneisen matrix
    chi = zeros(size(target_map));      % target absorber distribution
    
    %% Control position and density of absorbers

    if strcmp(p.Results.TargetCase, 'point') %point target case overrides all other options
        mu(target_map == 1) = p.Results.mu_properties(2);
        mu(target_map == 0) = p.Results.mu_properties(1); 
        
        rng(seed_number);
        g_background_rand = 0 + (p.Results.g_properties(1) + p.Results.g_properties(1))*rand(size(target_map));
        g_target_rand     = 0 + (p.Results.g_properties(2) + p.Results.g_properties(2)*rand(size(target_map)));  
        
        switch distribution_type
            case 'constant'
                chi = target_map;
                g(target_map == 1) = p.Results.g_properties(2);
                g(target_map == 0) = p.Results.g_properties(1); 
            case 'randomTarget'
                chi = target_map;
                g(target_map == 1) = p.Results.g_properties(2); %g_target_rand(target_map == 1);
                g(target_map == 0) = p.Results.g_properties(1); 
            case 'randomEverywhere'
                chi = target_map;
                g(target_map == 1) = p.Results.g_properties(2); %g_target_rand(target_map == 1);
                g(target_map == 0) = g_background_rand(target_map == 0);
        end        
    else   
        switch distribution_type
            case 'constant'
                % This case makes the target and background constant
                mu(target_map == 1) = p.Results.mu_properties(2); % target
                mu(target_map == 0) = p.Results.mu_properties(1); % background

                %if g is not reset, then we will see beam path in pressure profile
                g(target_map == 1) = p.Results.g_properties(2); %g_target;    
                g(target_map == 0) = p.Results.g_properties(1); %g_background;
                
                chi = target_map;
                
            case 'randomTarget'
                % This case makes the target have moderately dense absorbers, but the background be constant     

                chi = zeros(size(target_map));  % locations of absorbers

                density = p.Results.density; %              % density of absorbers [absorbers/m^2]
                area = sum(sum((target_map == 1)))*dx*dx;   % area of target region [m^2]

                px_per_m = 1/(dx*dx);                % [pixels/m2]            
                density_px = density/px_per_m;       % [absorbers per pixel]
                N_px = numel(target_map)*density_px; % absorbers
                                
                N = round(area.*density);                   % number of absorbers in the target area

                % Ensure at least 1 target is present
                if N == 0
                    N = 1;
                    sprintf('Target size was too small. Rounding number of absorbers to 1')
                end

                % Generate all possible coordinates for absorbers and filter center point out 
                [row,col] = find(target_map == 1);
                coordinate_pairs = [row,col];
                [idx] = ~ismember(coordinate_pairs,[p.Results.zo,p.Results.xo],'rows');  
                coordinate_pairs = coordinate_pairs(idx,:);
                
                if size(coordinate_pairs,1) < (N-1)
                    error('The chosen density is too dense for the chosen grid resolution. \n It would require %0.1f absorbers per pixel.',density_px);
                end
                
                % Randomly (w/o replacement) choose coordinate pairs
                rng(seed_number)
                stream = RandStream.getGlobalStream();           
                coordinates_chosen = datasample(stream,coordinate_pairs,N-1,1,'Replace',false); %choose N-1 coordinate (one absorber reserved for center)     

                for j = 1:N-1
                    chi(coordinates_chosen(j,1),coordinates_chosen(j,2)) = 1;
                end

                chi(p.Results.zo,p.Results.xo) = 1; %always put a absorber at the target center

                % fill in mu and g maps
                mu(chi ~= 1 & target_map == 0) = p.Results.mu_properties(1); % background absorbers only outside target
                mu(chi == 1) = p.Results.mu_properties(2);                  % target absorbers only inside target region 


                g(target_map == 1) = p.Results.g_properties(2); %g_target;    
                g(target_map == 0) = p.Results.g_properties(1); %g_background;

            case 'randomEverywhere'
                % This case makes optical phantom maps randomly vary in target and background    

                chi = zeros(size(target_map)); % locations of absorbers  

                density = p.Results.density; % density of absorbers [absorbers/m^2] %4e7;%/(3^2);    
                area = (size(target_map,1)*dx)*(size(target_map,2)*dx); % area of entire imaging field [m^2]
                
                px_per_m = 1/(dx*dx);                % [pixels/m2]            
                density_px = density/px_per_m;       % [absorbers per pixel]
                N_px = numel(target_map)*density_px; % absorbers
                
                N = round(area.*density);    % number of absorbers in the target area

                %ensure at least 1 target is present
                if N == 0
                    N = 1;
                    sprintf('Target size was too small. Rounding number of absorbers to 1')
                end

                %Generate all possible coordinates for absorbers and filter center point out
                [Z,W] = meshgrid(1:size(target_map,1),1:size(target_map,2));
                coordinate_pairs = [Z(:) W(:)];  %generate all possible coordinate pairs
                [idx] = ~ismember(coordinate_pairs,[p.Results.zo,p.Results.xo],'rows');  
                coordinate_pairs = coordinate_pairs(idx,:);
                
                if size(coordinate_pairs,1) < (N-1)
                    error('The chosen density is too dense for the chosen grid resolution. \n It would require %0.1f absorbers per pixel.',density_px);
                end
                
                % Randomly (w/o replacement) choose coordinate pairs
                rng(seed_number)
                stream = RandStream.getGlobalStream();
                coordinates_chosen = datasample(stream,coordinate_pairs,N-1,1,'Replace',false); %choose N-1 coordinate (one absorber reserved for center)     

                for j = 1:N-1
                    chi(coordinates_chosen(j,1),coordinates_chosen(j,2)) = 1;
                end

                chi(p.Results.zo,p.Results.xo) = 1; %always put a absorber at the target center

                % fill in maps
                mu(target_map == 1) = p.Results.mu_properties(2); % start by making target constant
                mu(target_map == 0) = p.Results.mu_properties(1); % start by making target constant background 
                %mu(chi ~= 1) = 0;
                mu = mu.*chi;                                      % randomly sample target and background regions so have absorbers or not
                
                %chi(target_map == 0) = 0;                         % reset chi to only show target absorber distribution
                
                rng(seed_number);
                g_background_rand = 0 + (p.Results.g_properties(1) + p.Results.g_properties(1))*rand(size(target_map));
                g_target_rand     = 0 + (p.Results.g_properties(2) + p.Results.g_properties(2))*rand(size(target_map));    

                g(target_map == 1) = g_target_rand(target_map == 1);
                g(target_map == 0) = g_background_rand(target_map == 0);
        end        
              
    end
    %% Visualize 
    if p.Results.PlotLayout
        x_axis = [0:size(mu,2)-1]*dx;
        z_axis = [0:size(mu,1)-1]*dx;
        
        figure;
        subplot(121);
        imagesc(x_axis*1e2,z_axis*1e2,mu);
        xlabel('cm');ylabel('cm');
        cb1 = colorbar;
        cb1.Label.String = '\mu_{a} (cm^{-1})';
        axis image
        
        subplot(122);
        imagesc(x_axis*1e2,z_axis*1e2,g);
        xlabel('cm');ylabel('cm');
        cb2 = colorbar;
        cb2.Label.String = 'Grueneisen';
        axis image
    end

end