%% Generates received pressure distribution

% INPUTS:
%   targetMap         --> (matrix) binary map of target location
%   chiMap            --> (matrix) target distribution map
%   muMap             --> (matrix) optical absorption distribution map
%   gMap              --> (matrix) Gruneisen parameter distribution map
%   fMap              --> (matrix) fluence distribution []
%   std_dev           --> TO DO: remove this as a variable
%   snrNoise          --> desired snr of inital pressure distribution
%                         NOTE: snrNoise = 0 means NO NOISE will be added
%   seed_number       --> (numeric) seed for random number generator
%   dx                --> (numeric) phantom resolution [m]

% INPUTS (optional):
%   'ZeroMean'        --> (logical) choose whether or not to zero mean the
%                         pressure distribution after noise addition (default = true)
%   'PlotLayout       --> (logical) choose whether or not to plot target distribution (default = true)

% OUTPUTS
%   pMap               --> (matrix) raw pressure distribution
%   pMap_smooth        --> (matrix) smoothed pressure distribution
%   N_sc               --> (numeric) ??? TODO:recall what this variable is and its use or remove
%   noise              --> (matrix) noise matrix 

function [pMap,pMap_smooth,N_sc,noise] = makePressure(targetMap, chiMap, muMap, gMap, fMap, snrNoise, seed_number, dx, varargin)
    
    p = inputParser();
    
    addParameter(p,'PlotLayout',true);
    addParameter(p,'ZeroMean',true);
    
    parse(p,varargin{:});
    
    A = gMap.*muMap.*fMap;             % definition of photoacoustic pressure distribution at the absorber surface  
    A0 = A - mean2(A(targetMap == 0)); % remove DC component of background
    
    if ~isnan(snrNoise) % ~= 0
        signal_mean = max(max(A0(targetMap == 1)));
        std_dev = signal_mean ./ db2mag(snrNoise);

        rng(seed_number);
        noise = randn(size(A0)).*std_dev; 
    else
        noise = 0;
        std_dev = 0;
    end


    pMap = (A0.^2) + 2.*noise.*(max(max(A0(targetMap == 1))).^2) + std_dev.*(mean(mean((A0(targetMap == 1)))).^2);

    N_sc = std_dev.*(mean(mean((A0(targetMap == 1)))).^2);
    
    pMap = pMap./max(pMap(:));
    %% Re zero mean the data
    if p.Results.ZeroMean
         for i = 1:size(pMap,1)
             if sum(targetMap(i,:)) ~= 0
                 pMap(i,:) = pMap(i,:) - mean(pMap(i,(targetMap(i,:) == 0))); %subtract background mean everywhere
             else
                pMap(i,:) = pMap(i,:) - mean(pMap(i,:));
             end
         end
    end
    
    %% Smooth Pressure
    %multiply by gaussian point spread function
    psf_x = [-(size(pMap,2)/2):1:(size(pMap,2)/2)-1];
    psf_z = [-(size(pMap,1)/2):1:(size(pMap,1)/2)-1];      

    Sigma = [1,0;
             0,1]; %define gaussian covariance matrix                                       

    [X1,X2] = meshgrid(psf_x,psf_z);
    psf = mvnpdf([X1(:) X2(:)],[0,0],Sigma);
    psf = reshape(psf,length(psf_z),length(psf_x));    
    psf = psf./max(psf(:));   

    pMap_smooth = conv2(pMap,psf,'same');  %convolve pressuremap with gaussian to smooth
       
    pMap_smooth = pMap_smooth./max(pMap_smooth(:));

    %% Visualize
    if p.Results.PlotLayout
        x_axis = [0:size(pMap,2)-1]*dx;
        z_axis = [0:size(pMap,1)-1]*dx;
    
        figure('Position',[100,100,1600,500]);
        subplot(131);
        imagesc(x_axis*1e2,z_axis*1e2,pMap);
        xlabel('cm');ylabel('cm');title('Pressure Map');axis image;colormap jet
        
        subplot(132);
        imagesc(x_axis*1e2,z_axis*1e2,pMap_smooth);
        xlabel('cm');ylabel('cm');title('Pressure Map - Smooth');axis image;colormap jet
        
        subplot(133);
        imagesc(x_axis*1e2,z_axis*1e2,pMap - pMap_smooth);
        xlabel('cm');ylabel('cm');title('Pressure Map Differences');axis image;colormap jet
        
        figure;
        plot(x_axis*1e2,pMap(round(size(pMap,1)/2),:));
        xlabel('cm');ylabel('Pa');
        title('Pressure Profile \newline @ target center');
    end

end