%% Generates fluence distribution

% INPUTS
%   targetMap         --> (matrix) binary map of target location
%   fiberWidth        --> (numeric) diameter of fiber [m]
%   zo                --> (numeric) target center, z coordinate [gridpoints]
%   dx                --> (numeric) phantom resolution [m]

% INPUTS (optional)
%   'FiberZ'           --> (numeric) distance between fiber tip and target center [m]
%   'PlotLayout        --> (logical) choose whether or not to plot target distribution (default == true)

% OUTPUTS
%   fMap               --> (matrix) fluence distribution []


function [fMap] = makeFluence(targetMap,fiberWidth,dx,varargin)
%% Parse inputs
p = inputParser;

defaultFiberZ = 12e-3/2;  %distance between fiber tip and target center [m]
%addParameter(p,'FiberZ',defaultFiberZ,@isnumeric);
addParameter(p, 'PlotLayout',true);
addParameter(p, 'FiberDepth',1,@isnumeric);
addParameter(p, 'Attenuation',false,@islogical);

parse(p,varargin{:})    %parse the inputs

%% Set defaults
Fo = 5e-3;        %fluence [J]
z = size(targetMap,1);

if isinf(fiberWidth) %use light sheet
    fMap = Fo*ones(size(targetMap));
else %make gaussian beam
    Io = Fo;                   % peak beam intensity [W/m^2]
    wo = fiberWidth/dx;        % wo = w(0) = minimum beam radius [samples]
    na = 0.39;                 % numerical aperture
    theta = 2*asin(na);        % divergence angle in [degrees]
    
    zr = wo/theta;             % Rayleigh length
    
    %r_vec = -round(length(xoi)/2)+1:round(length(xoi)/2);
    r_vec = -round(size(targetMap,2)/2)+1:round(size(targetMap,2)/2);
    
    zfstart = p.Results.FiberDepth; %zo - (p.Results.FiberZ)/dx;      %zfstart = (zo-r); %location of fiber tip [px]
    
    wz = round(wo*sqrt(1 + ((1:(z-zfstart)+1)./zr).^2));             % beam width at each depth
    beam = Io.*exp(-2*(repmat(r_vec,size(wz,2),1).^2)./(repmat(wz',1,size(r_vec,2))).^2); %gaussian function at each depth    
    
    fMap(zfstart:z,:) = beam;
end

if p.Results.PlotLayout
    x_axis = [0:size(fMap,2)-1]*dx;
    z_axis = [0:size(fMap,1)-1]*dx;

   figure;
   imagesc(x_axis*1e2,z_axis*1e2,fMap);
   xlabel('cm');ylabel('cm');
   cb = colorbar;
   cb.Label.String = 'Fluence'; %sprintf('Fluence-%0.1f mm',fiberWidth*1e3);
   axis image
end
end