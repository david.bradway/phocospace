%% Generates random, moderately dense absorber distribution based on binary targetMap

% INPUTS
%   targetMap         --> (matrix) binary map of target location
%   dx                --> (numeric) phantom resolution [m]
%   zo                --> (numeric) target center, z coordinate [gridpoints]
%   xo                --> (numeric) target center, x coordinate [gridpoints] 
%   r                 --> (numeric) target radius [m]
%   distribution_type            --> (string) type of target distribution, options are
%                         'constant', 'constandDistribution', 'randomTarget', 
%                         'randomEverywhere','kWave' (TODO: kWave option does not work)

% INPUTS (optional)
%   'Kname'            --> (string) name of kwave file to mimic distribution of
%   'PlotLayout        --> (logical) choose whether or not to plot target distribution (default == true)

% OUTPUTS
%   chi                --> (matrix) target distribution map

function [chi] = makeChi(targetMap,dx,r,distribution_type,seed_number,varargin)

%% Parse inputs
p = inputParser;

defaultKname = 'none'; 
addParameter(p,'Kname',defaultKname,@isstruct);
addParameter(p,'PlotLayout',true);
addParameter(p,'Density',4e7); % [absorbers/(m^2)]  
addParameter(p,'xo',round(size(targetMap,2)/2),@isnumeric);
addParameter(p,'zo',round(size(targetMap,1)/2),@isnumeric);

parse(p,varargin{:})    %parse the inputs

kname = p.Results.Kname;


%% Generate distribution
switch distribution_type
    case 'constant'
        chi = 1;   
        p.Results.Density = 0;
    
    case 'constantDistribution'
        chi = ones(size(targetMap));
        %p.Results.Density = inf;
        
    case 'constantTarget'
        chi = zeros(size(targetMap));  
        chi(targetMap) = 1;
        
        
    case 'randomTarget'
        chi = zeros(size(targetMap));  
        
        %Density = 4e7;%/(3^2);       % [absorbers/(m^2)]          
        %if strcmp(shape,'circle')
        %    area = pi*(r)^2;             % [m^2]
        %    N = round(area.*p.Results.Density);    % number of absorbers in the target area
        %else
        % Determine number of absorbers       
        area = sum(targetMap(:))*dx*dx;
        N = round(area.*p.Results.Density);        
        %end
        
        % Ensure at least 1 target is present
        if N == 0
            N = 1;
            sprintf('Target size was too small. Rounding number of absorbers to 1')
        end
     
        rng(seed_number);
        
        % Generate all possible coordinates for absorbers and filter center point out 
        [row,col] = find(targetMap == 1);
        coordinate_pairs = [row,col];
        [idx] = ~ismember(coordinate_pairs,[p.Results.zo,p.Results.xo],'rows');  %filter out center point
        coordinate_pairs = coordinate_pairs(idx,:);
        
        % random choose absorber positions
        coordinates_chosen = datasample(coordinate_pairs,N-1,1,'Replace',false); %choose N-1 coordinate (one absorber reserved for center)     
        
        % Randomly (w/o replacement) choose coordinate pairs
        for j = 1:N-1
            chi(coordinates_chosen(j,1),coordinates_chosen(j,2)) = 1;
        end
        
        chi(p.Results.zo,p.Results.xo) = 1; %always put a absorber at the target center
        
    case 'randomEverywhere'            
        chi = zeros(size(targetMap));  
      
        %Density = 4e7;%/(3^2);                                %[absorbers/m^2]
        area = (size(targetMap,1)*dx)*(size(targetMap,2)*dx); %[m^2]
             
        N = round(area.*p.Results.Density);    %number of absorbers in the target area
        
        %ensure at least 1 target is present
        if N == 0
            N = 1;
            sprintf('Target size was too small. Rounding number of absorbers to 1')
        end

        rng(seed_number);

        %Generate all possible coordinates for absorbers and filter center point out
        [Z,W] = meshgrid(1:size(targetMap,1),1:size(targetMap,2));
        coordinate_pairs = [Z(:) W(:)];  %generate all possible coordinate pairs
        [idx] = ~ismember(coordinate_pairs,[p.Results.zo,p.Results.xo],'rows');  
        coordinate_pairs = coordinate_pairs(idx,:);
        
        % Randomly (w/o replacement) choose coordinate pairs
        coordinates_chosen = datasample(coordinate_pairs,N-1,1,'Replace',false); %choose N-1 coordinate (one absorber reserved for center)     
        
        for j = 1:N-1
            chi(coordinates_chosen(j,1),coordinates_chosen(j,2)) = 1;
        end
        
        chi(p.Results.zo,p.Results.xo) = 1; %always put a absorber at the target center
        
    case 'kWave'
        % TODO: this option is outdated and does not work
        chi = zeros(size(targetMap));
        
        if strcmp(kname,'none')
            load(uigetfile('*.mat'));
        else
            %load(kname);
            slsc_kwave = kname;
        end
        
        dxkwave = dx;
        dykwave = (slsc_kwave.ax(2)-slsc_kwave.ax(1));
        
        if size(slsc_kwave.target,1) ~= size(targetMap,1)
            [x,y] = meshgrid((0:length(slsc_kwave.lat)-1).*(dxkwave),(0:length(slsc_kwave.ax)-1).*dykwave);
            [XI,YI] = meshgrid([0:dx:x(end)],[0:dx:y(end)]);
            slsc_kwave.target = interp2(x,y,slsc_kwave.target,XI,YI);
            slsc_kwave.target(slsc_kwave.target < 0.05) = 0;
        end       
        chi = padarray(slsc_kwave.target,[floor((size(targetMap,1)-size(slsc_kwave.target,1))/2),...
            round((size(targetMap,2)-size(slsc_kwave.target,2))/2)],0,'both');
end

%% Visualize
if p.Results.PlotLayout
    %figure;imagesc(chi);title(sprintf('Absorber Distribution - Density %d m^{2}',Density));
    x_axis = [0:size(chi,2)-1]*dx;
    z_axis = [0:size(chi,1)-1]*dx;
    figure;
    imagesc(x_axis*1e2,z_axis*1e2,chi);title(sprintf('Absorber Distribution'));
    xlabel('cm');ylabel('cm');
    axis image;
end
end