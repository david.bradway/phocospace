%% Creates binary map of target position

% INPUTS:
%   phantom_grid      --> matrix of zeros of size of imaging plane
%   shape             --> (string) target shape, options are 'circle' and
%                         'square'
%   dx                --> (numeric) resolution (symmetric) of imaging field [m]

% INPUTS (optional):
%   TargetCenter      --> (numeric) array of target center coordinates [x center, z center] [gridpoints]
%   radius            --> (numeric) radius of target [m]
%   PlotLayout        --> (logical) choose whether or not to plot target distribution (default == true)

% OUPUTS:
%   target_map        --> (numeric) binary map of target_map location
%   target_case       --> (string) indicates if target is normal or the special point target case


function [target_map,target_case] = makeTarget(phantom_grid,shape,dx,varargin) %(res_x, x_length, z_length, shape, locx, r)
    
    %% Parse variable-length inputs
    p = inputParser();
    
    addParameter(p,'PlotLayout',true,@islogical);
    addParameter(p,'Radius',0.002,@isnumeric);          %[m]
    addParameter(p,'TargetCenter',[0,0],@isnumeric);
    
    parse(p,varargin{:}); 
    
    if p.Results.Radius <= (dx)/2
        target_case = 'point';
    else
        target_case = 'normal';
    end
    
 
    %% Define target region     
    % define a circle of radius r and centerpoint (xo,zo)
    r = round(p.Results.Radius/dx);     %target radius [grid points]
    
%     xo = round(size(phantom_grid,2)/2); %[gridpoints]
%     zo = round(size(phantom_grid,1)/2); %[gridpoints]
    
    xcoords = [-(round(size(phantom_grid,2)/2))+1:1:round(size(phantom_grid,2)/2)];   %[gridpoints]
    zcoords = [-(round(size(phantom_grid,1)/2))+1:1:round(size(phantom_grid,1)/2)];   %[gridpoints]   
    
    [XC,YC] = meshgrid(xcoords,zcoords); 
    
    if strcmp(shape,'circle')
        target_map = (((YC - p.Results.TargetCenter(2)).^2 + (XC - p.Results.TargetCenter(1)).^2) <= r^2);
    elseif strcmp(shape, 'square')
        target_map = (abs(YC - p.Results.TargetCenter(2)) <= r/2) & (abs(XC-p.Results.TargetCenter(1)) <= r/2);
    else
        fprintf('You chose an unacessible shape \n');
    end
    
    if strcmp(target_case,'point')
        target_map = zeros(size(phantom_grid));
        target_map(round(size(phantom_grid,1)/2),round(size(phantom_grid,2)/2)) = 1;
    end
    
  
    %% Visualize target
    if p.Results.PlotLayout
        figure;
        imagesc(xcoords.*dx.*1e2,zcoords.*dx.*1e2,target_map);
        %title('Target Location','interpreter','Latex');
        set(gca,'FontSize',15)
        xlabel('Position (cm)','interpreter','Latex','FontSize',15);ylabel('Position (cm)','interpreter','Latex','FontSize',15);
        axis image;
    end 
end