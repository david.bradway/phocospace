%% Adjusts the defined imaging plane dimensions such that target can always be centered

% INPUTS:
%   w           --> phantom width [m]
%   z           --> phantom depth [z]
%   dx          --> resolution (symmetric) of imaging field [m]

% OUTPUTS
%   phantom_grid  --> matrix of zeros
%   w             --> new phantom width [m]
%   z             --> new phantom depth [m]

function [phantom_grid,w,z] = makePhantomGrid(w,z,dx)
    
    w_gd = round(w/dx);
    z_gd = round(z/dx);
    
    % check if phantom width is odd, and make it even
    if mod(w_gd,2) ~= 0
        w_gd = w_gd +1;
        w = w_gd*dx;
    end
    
    % check if phantom depth is odd, and make it even
    if mod(z_gd,2) ~= 0
        z_gd = z_gd +1;
        z = z_gd*dx;
    end
        
    phantom_grid = zeros(z_gd,w_gd);
    
end