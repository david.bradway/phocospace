%% Makes a strcuture with required transducer parameters

% INPUTS:
%   none required

% INPUTS (OPTIONAL):
%   Frequency           --> vector of 6 dB transducer bandwith [MHz]
%   CenterFrequency     --> center frequency [MHz]
%   Pitch               --> spacing between elements [m]
%   NumElements         --> number of elements
%   Response            --> weighted frequency response 
%                           default = 1 which means uniform weighting
%                           if not 1, the reponse vector must have the same
%                           number of elements as the frequency vector
% NOTE!!!! default inputs are based on the Alpinion L3-8 ultrasound probe

% OUTPUTS:
%   transducer          --> struct containing transducer parameters
%   w                   --> lateral length of transducer [m]

function [transducer,w] = makeTransducer(varargin)
   
    %%%%%%
    p = inputParser;
    
    addParameter(p,'Frequency',[3.75:0.25:8.0]);  %[MHz]
    addParameter(p,'CenterFrequency',5.5);        %[MHz]
    addParameter(p,'Pitch',0.3e-3);               %[m]
    addParameter(p,'NumElements',128)             %
    addParameter(p,'Response',1); %10.^(([-50.26,-48.36,-47.32,-46.54,-46.28,...
                                     %-45.50,-45.07,-44.64,-44.29,-44.73,...
                                     %-45.76,-46.54,-48.44,-50.56] + 44.29)/20)); 
                                     % weights empirically based on Alpinion L3-8 transducer frequency response [dB]
    parse(p,varargin{:});
    
    
    %%%%%%
    transducer = struct();
    
    transducer.Frequency            = p.Results.Frequency;
    transducer.CenterFrequency      = p.Results.CenterFrequency;
    transducer.Pitch                = p.Results.Pitch;
    transducer.NumElements          = p.Results.NumElements;
    transducer.Response             = p.Results.Response;
    
    %
    if transducer.Response == 1
        transducer.Response = ones(1,numel(transducer.Frequency));
    else
        if numel(transducer.Response) ~= numel(transducer.Frequency)
            error('Frequency vector and frequency response weighting vector must have the same number of elements.')
        end
    end
    
    % compute width of imaging field
    w = transducer.Pitch*transducer.NumElements;  %[m]

end