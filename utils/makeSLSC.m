%% Execute theoretical photoacoustic slsc algorithm

% INPUTS:
%   targetMap         --> (matrix) binary map of target location
%   pMap              --> (matrix) raw pressure distribution
%   dx                --> (numeric) phantom resolution [m]
%   mton              --> (numeric) conversion factor, [meters/pixel]
%   target_case       --> (string) choose if simulation is for a point
%                           target, options are 'point' and 'normal (defult = 'normal')
%   sim_range         --> (string), choose to simulated image ('image') or one
%                          profile at depth of center of target ('line') 

% INPUTS (optional):

% OUTPUTS
%   slsc_theory       --> (struct) containing:
%           cc          --> (matrix) lateral location x lag x frequency
%           cc_avg      --> (matrix) lateral location x lag
%           slsc        --> (matrix) depth x lateral x lag x frequency
%           slsc_avg    --> (matrix) depth x lateral x lag
%           maxM        --> (numeric) maximum lag simulated
%           pitch       --> (numeric) transducer pitch aka pixel width
%           mton        --> (numeric) conversion factor [meters/pixel]
%           N_ele       --> (numeric) number of transducer elements
%           freq        --> (array) frequencies used
%           x_axis      --> (array) lateral axis [m]
%           z_axis      --> (array) depth axis [m]


function [slsc_theory] = makeSLSC(pMap,targetMap,transducer,dx,target_case,varargin)

    n_ele = transducer.NumElements; %128;                       % number of elements 
    mton = 1/dx;
        
    p = inputParser;
    
    addParameter(p,'SimRange','image');
    addParameter(p,'DepthOfInterest',round(size(targetMap,1)/2),@isnumeric);
    addParameter(p,'maxM',n_ele-1,@isnumeric);
        
    
    parse(p, varargin{:});
    
    %% Extrapolate on transducer parameters 
    c = 1480;                           % average speed of sound in imaging medium [m/s]  
       
    freq = transducer.Frequency;        % ultrasound transducer frequency vector [MHz] 
    lam = c./(freq.*1e6);               % ultrasound transducer wavelength vector [m] 
    lam_center = c./(transducer.CenterFrequency.*1e6); % ultrasound transducer center wavelength [m]    
    pitch = transducer.Pitch;           % ultrasound transducer pitch [m]
      
    % Define a very large transducer
    %n_ele = transducer.NumElements;
    n_ele_large = transducer.NumElements*2;%256;%128;            % number of elements
    D_large = pitch*n_ele_large;       % aperture width [m]
    D_px_large = round(D_large/dx);    % aperture width [px]

    % Define the size of the portion of transducer we are using
    D = pitch*n_ele;                   % aperture width [m]
    D_px = round(D/dx);                % aperture width [px]
    locx = round(size(targetMap,2)/2); % lateral center of phantom [px]
    xoi = (locx - round(D_px/2)+1:locx + round(D_px/2));   
    D_vec = 0:pitch:(D-pitch);        % aperture width vector [m]
    len = round((D_px)/2)+0; % half of aperture width [px]
                
    %% Select if simulating all depths, or only z = depth/2
    switch p.Results.SimRange
        case 'image'
            axialRange = 1:size(pMap,1);
        case 'line'
            if mton == 10000
                axialRange = round(size(targetMap,1)/2);
                axialRange = [axialRange,axialRange];
            elseif mton == 50000
                axialRange = round(size(targetMap,1)/2);
            else
                axialRange = round(size(targetMap,1)/2);
            end
    end
        
    %% Calculate SLSC

    %Create spatial frequency axis (x-axis after fourier transform)
    ks = mton;                          % sampling freq [pixels/meter]
    kx = -ks/2:ks/(D_px-1):ks/2;        % spatial frequency vector [1/m]
    omega = 2*pi*kx;                    % spatial frequency vector [radians] equivalent to omega = 2*pi*(scale4*pitch)/(lambda1*foc)

    cc = zeros(n_ele,n_ele,length(freq));     %initialize coherence function matrix [no. elements x no. elements x no. freq]
    slsc = zeros(size(pMap,1),length(1:n_ele),p.Results.maxM,length(freq)); %initialize slsc matrix [depth x width x lag x no. freq]
    
    %% Calculate coherence function and corresponding slsc pixel value at each depth in axialRange
    for zoi = axialRange %300;%1:size(mu,1) %zo:zo;
        paprofile = pMap(zoi,xoi);
        ft = (1/(zoi*dx)^2).*fftshift(fft(paprofile));    %fourier transform to yield spatial coherence function (large aperture to prevent aliasing)
                
        % Generate matrix of coherence functions at each lateral position for a singular depth
        if strcmp(target_case,'point') %point') %|| sum(targetMap(zoi,xoi)) == 2 % ||sum(targetMap(zoi,xoi)) == 0
            nx = -(D/2):pitch:(D/2)-pitch;                    % aperture width vector centered around 0 [m]            
            phase = exp(sqrt(-1).*omega.*nx');                % phase term [lateral position x spatial frequency vector]                                                           
            cx = (repmat(abs(ft),[length(nx),1])).*(phase); % shifted coherence functions [no elements x spatial freq vector]
                                                              % mulitplication by phase implements what is drawn in Fig 2 of TUFFC paper
        else
            nx = 0:pitch:D-pitch;                       % aperture width vector [m] (same as variable D_vec)
            phase = exp(sqrt(-1).*omega.*nx');          % phase term [lateral position x spatial frequency vector]
            cx = repmat(ft,[length(nx),1]).*phase;      % shifted coherence functions [lateral position x spatial freq vector]
                                                        % mulitplication by phase implements what is drawn in Fig 2 of TUFFC paper
        end
        
        cx = (cx./repmat(max(cx,[],2),[1,size(cx,2)]));                            % normalized coherence function (to ensure no values are above 1)
        
        % Iterate over all discrete frequencies in the probe bandwidth
        for k = 1:length(freq)
            
            focus = 0.03; %arbitrary focus, zoi*dx [m]      
            xx = kx.*(lam(k)*focus); % lag axis [m] (Eq. 22 in TUFFC paper)
            
            % interpolate coherence function from positive half of spatial frequency axis (kx) to lag axis (xx)
            cn(:,:,k) = (interp1(xx(len:end)/pitch,cx(:,len:end)',D_vec/pitch,'linear'))'; % coherence function [no. elements x lag]

            cn(:,:,k) = (cn(:,:,k)./repmat(max(cn(:,:,k),[],2),[1,size(cn,2)])); % normalized coherence function after interpolation
            
            %define cc for .m file output as the coherence functions at the axial center of the phantom
            if zoi == p.Results.DepthOfInterest   
                cc(:,:,k) = real(cn(:,:,k)); %only take the real part --? for display but also mutual coherence is defined as the real part?
            end
        end

        % Calculate SLSC image at all lags by summing coherence functions up to a indicated lag 
        cn = real(cn);           % real part of coherence function
        cctmp = cn(:,1,:);       % take coherence curves at lag 1
        slsc(zoi,:,1,:) = cctmp; % % make lag 1 slsc image have value of coherence curve at lag 2
        
        % Iterate over all lags to calculate slsc profile at each lag
        for m = 2:p.Results.maxM                   % start at index 2 to remove DC component
            cctmp = cctmp + cn(:,1+m,:); % update cctmp to be cumulative sum up to m
            slsc(zoi,:,m,:) = cctmp;
        end       
                    
    end
    
    % Weight coherence functions by transducer frequency response
    for i = 1:size(slsc,4)
        slsc(:,:,:,i) = transducer.Response(i)*slsc(:,:,:,i); % weighted slsc images
    end
    
    slsc_avg = sum(slsc,4)./size(slsc,4); % slsc image averged over the transducer frequency response
    slsc_avg(isnan(slsc_avg)) = 0; %remove any nans from slsc image (i.e. remove all divide by zeros)      
    cc_avg = squeeze(sum(cc(:,:,:),3)./(length(freq))); % coherence functions averaged over transducer frequency response
    
    lat = 0:pitch:((transducer.NumElements-1)*pitch);        % define lateral axis [m]
    ax  = 0:dx:((dx*size(pMap,1))-dx); % define depth axis [m]
    
    % create output struct
    slsc_theory = struct('cc',cc,'cc_avg',cc_avg,'slsc',slsc,'slsc_avg',slsc_avg,'maxM',p.Results.maxM,'pitch',pitch,...
        'mton',mton,'N_ele',n_ele,'freq',freq,'x_axis',lat,'z_axis',ax);

end