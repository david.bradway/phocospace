%% Demonstrate default target maps and associated SLSC images

clear all
close all 
clc

addpath(genpath('utils'));
f1 = figure('Position',[386 30 1135 934]);
set(0,'defaultAxesFontSize',12);

subplot_idx_C1 = 1;
subplot_idx_C2 = 2;

target_map_cases = {'circle_4mm';
                    'polka_dots.mat';
                    'vasculature_kwave.mat';
                    'jhu_crest.mat'};

s = cell(numel(target_map_cases),2); %cell to store subplot axes for modification

for i = 1:numel(target_map_cases)
   

    load(fullfile('target_maps',target_map_cases{i}));    
    target_case = 'normal';
    seed_number = 1;
    
    %% Plot target geometry
    figure(f1);
    subplot(4,2,subplot_idx_C1);
    imagesc([0:dx:w].*1e3,[0:dx:z].*1e3,target_map);colormap gray;
    ylabel('mm');
    axis image
    
    ax = gca;
    s{i,1} = ax; 
    
    %% Make transducer
    [transducer,~] = makeTransducer();
    
    %% Make optical properties/absorber distribution
    [mu_map, g_map, chi_map, target_map] = makeOpticalPhantom(target_map,'randomEverywhere',dx,seed_number,...
                                           'TargetCase',target_case,'PlotLayout',false);

    %% Make fluence distribution
    fiber_width = inf; %5e-3; %[m]
    fiber_depth = round(size(target_map,1)/2) - 12e-3/2/dx;
    [f_map] = makeFluence(target_map,fiber_width,dx,...
                          'FiberDepth',fiber_depth,...
                          'Attenuation',true,...
                          'PlotLayout',false);


    %% Make pressure map
    snr = 10; %25;    % desired SNR of initial pressure distribution [dB] (note snr = nan means no noise is added)
    [~,pMap_smooth,~] = makePressure(target_map, chi_map, mu_map, g_map, f_map,...
                                     snr, seed_number, dx,...
                                     'ZeroMean', true, 'PlotLayout', false);

    %% Run theoretical SLSC algorithm
    sprintf('Calculating SLSC')

    % =========================================================================
    % SIMULATION
    % =========================================================================
    maxM = 30;
    tic
    [slsc_theory] = makeSLSC(pMap_smooth, target_map, transducer,dx,target_case,...
                             'SimRange', 'image',...
                             'maxM',maxM);
    toc
    
    %% Plot theoretical SLSC image
    subplot(4,2,subplot_idx_C2);
    M = 14;
    imagesc([0:dx:w].*1e3,[0:dx:z].*1e3,slsc_theory.slsc_avg(:,:,M)./max(max(slsc_theory.slsc_avg(:,:,M))),[0,1]);colormap gray;
    ylabel('mm');
    axis image
    
    cb = colorbar;
    cb.Ticks = [0 0.5 1.0];
    cb.TickLabels = {'0','0.5','1'};
    
    ax = gca;
    s{i,2} = ax;
        
    % update plotting indexes
    subplot_idx_C1 = subplot_idx_C1 + 2;
    subplot_idx_C2 = subplot_idx_C2 + 2;
end
pause(0.01);
s{1,2}.Position = [s{1,2}.Position(1) - 0.18, s{1,2}.Position(2) + 0.025*0, s{1,2}.Position(3),s{1,2}.Position(4)];
s{2,2}.Position = [s{2,2}.Position(1) - 0.18, s{2,2}.Position(2) + 0.025*1, s{2,2}.Position(3),s{2,2}.Position(4)];
s{3,2}.Position = [s{3,2}.Position(1) - 0.18, s{3,2}.Position(2) + 0.025*2, s{3,2}.Position(3),s{3,2}.Position(4)];
s{4,2}.Position = [s{4,2}.Position(1) - 0.18, s{4,2}.Position(2) + 0.025*3, s{4,2}.Position(3),s{4,2}.Position(4)];
s{2,1}.Position = [s{2,1}.Position(1) , s{2,1}.Position(2) + 0.025*1, s{2,1}.Position(3),s{2,1}.Position(4)];
s{3,1}.Position = [s{3,1}.Position(1) , s{3,1}.Position(2) + 0.025*2, s{3,1}.Position(3),s{3,1}.Position(4)];
s{4,1}.Position = [s{4,1}.Position(1) , s{4,1}.Position(2) + 0.025*3, s{4,1}.Position(3),s{4,1}.Position(4)];

s{1,2}.YAxis.Visible = 'off';
s{2,2}.YAxis.Visible = 'off';
s{3,2}.YAxis.Visible = 'off';
s{4,2}.YAxis.Visible = 'off';


s{4,1}.XLabel.String = 'mm\newline (g)';  
s{4,2}.XLabel.String = 'mm\newline (h)';  
s{3,2}.XTick = 38.4/2;s{3,2}.XTickLabel = '(f)';
s{3,1}.XTick = 38.4/2;s{3,1}.XTickLabel = '(e)';
s{2,2}.XTick = 38.4/2;s{2,2}.XTickLabel = '(d)';
s{2,1}.XTick = 38.4/2;s{2,1}.XTickLabel = '(c)';
s{1,2}.XTick = 38.4/2;s{1,2}.XTickLabel = '(b)';
s{1,1}.XTick = 38.4/2;s{1,1}.XTickLabel = '(a)';

s{1,1}.Title.String = 'Target Geometry';
s{1,2}.Title.String = 'Theoretical SLSC Image';
