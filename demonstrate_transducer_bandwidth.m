%% Demonstrate default target maps and associate SLSC images

clear all
close all 
clc

addpath(genpath('utils'));
f1 = figure('Position',[204 347 1569 457]);
set(0,'defaultAxesFontSize',12);

subplot_idx_C1 = 1;
subplot_idx_C2 = 2;

frequency          = {[8:0.25:17.0];
                      [3:0.25:8.0];
                      [1:0.25:5.0]}; % frequency vectors [MHz]
                  
frequency_c        = {12.0;
                      5.5;
                      3.0}; % center frequencies [MHz]
                
s = cell(numel(frequency)+1,1); %cell to store subplot axes for modification

for i = 1:numel(frequency)
       
    load(fullfile('target_maps','vasculature_kwave.mat'));
    
    if i == 1
        subplot(1,4,4);
        hold on
        plot(([0:dx:0.0384-dx]-dx*4).*1e3,target_map(81,:));%num2str(freq_c{i});
        ylabel('Pixel Amplitude');
        hold off
    end
    
    target_case = 'normal';
    seed_number = 1;
    
    %% Make transducer
    [transducer,~] = makeTransducer('Frequency',frequency{i},'CenterFrequency',frequency_c{i});
    
    %% Make optical properties/absorber distribution
    [mu_map, g_map, chi_map, target_map] = makeOpticalPhantom(target_map,'randomEverywhere',dx,seed_number,...
                                           'TargetCase',target_case,'PlotLayout',false);

    %% Make fluence distribution
    fiber_width = inf; %5e-3; %[m]
    fiber_depth = round(size(target_map,1)/2) - 12e-3/2/dx;
    [f_map] = makeFluence(target_map,fiber_width,dx,...
                          'FiberDepth',fiber_depth,...
                          'Attenuation',true,...
                          'PlotLayout',false);
                                            
    %% Make pressure map
    snr = 25; %10; %25;    % desired SNR of initial pressure distribution [dB] (note snr = nan means no noise is added)
    [~,pMap_smooth,~] = makePressure(target_map, chi_map, mu_map, g_map, f_map,...
                                     snr, seed_number, dx,...
                                     'ZeroMean', true, 'PlotLayout', false);

    %% Run theoretical SLSC algorithm
    sprintf('Calculating SLSC')

    % =========================================================================
    % SIMULATION
    % =========================================================================
    maxM = 30;
    tic
    [slsc_theory] = makeSLSC(pMap_smooth, target_map, transducer,dx,target_case,...
                             'SimRange', 'image',...
                             'maxM',maxM);
    toc
    
    %% Plotting
    M = 14;
    
    % Plot theoretical SLSC image
    subplot(1,4,i);
    imagesc([0:dx:w].*1e3,[0:dx:z].*1e3,slsc_theory.slsc_avg(:,:,10)./max(max(slsc_theory.slsc_avg(:,:,10))),[0 1]);colormap gray;
    ylabel('mm');%xlabel('(mm)');
    axis image
    
    ax = gca;
    s{i} = ax;
    
    % Plot lateral SLSC profile at z = 8.1 mm
    subplot(1,4,4);
    hold on
    plot(slsc_theory.x_axis.*1e3,squeeze(slsc_theory.slsc_avg(81,:,10))./max(slsc_theory.slsc_avg(81,:,10)));%num2str(freq_c{i});
    ylabel('Pixel Amplitude');
    xlim([25.8-5.3, 25.8+5.3]);
    hold off
    ax = gca;
    s{4} = ax;
    
    % update plotting indexes
    subplot_idx_C1 = subplot_idx_C1 +2;
    subplot_idx_C2 = subplot_idx_C2 +2;
end

%% Adjust subplot positioning

s{1}.Position = [s{1}.Position(1) - 0.02*0, s{1}.Position(2), s{1}.Position(3),s{1}.Position(4)];
s{2}.Position = [s{2}.Position(1) - 0.02*1, s{2}.Position(2), s{2}.Position(3),s{2}.Position(4)];
s{3}.Position = [s{3}.Position(1) - 0.02*2, s{3}.Position(2), s{3}.Position(3),s{3}.Position(4)];
s{4}.Position = [s{4}.Position(1) - 0.02*3, s{3}.Position(2), s{3}.Position(3),s{3}.Position(4)];
s{4}.Position = [0.7,0.34,0.22,0.36];

s{2}.YAxis.Visible = 'off';
s{3}.YAxis.Visible = 'off';

s{1}.XLabel.String = 'mm\newline (a)';  
s{2}.XLabel.String = 'mm\newline (b)';  
s{3}.XLabel.String = 'mm\newline (c)';  
s{4}.XLabel.String = 'mm\newline (d)';  

s{4}.Title.String = 'Lateral SLSC Profiles (z=8.1mm)'; %'1-5 MHz';  
s{3}.Title.String = '8-17 MHz'; %'Lateral SLSC Profiles (z=8.1mm)';  
s{2}.Title.String = '3-8 MHz';  
s{1}.Title.String = '1-5 MHz'; %'8-17 MHz';  

legend({'Target Map','8-17 MHz','3-8 MHz','1-5 MHz'});
