%% Example script for how to run the theoretical photoaocustic SLSC simulations

% author: Michelle T. Graham

clearvars;
close all
clc;

set(0, 'defaultlinelinewidth',3,...
       'defaultaxesfontsize',16,...
       'defaultlinemarkersize',4); %set plotting display defaults

addpath(genpath(fullfile('utils'))); % add path to theory utilities


%% Define imaging field size
[transducer,w] = makeTransducer();
z  = 0.0256;   % depth of imaging field                  [m]
dx = 1e-4;     % grid resolution (symmetric) of imaging field [m] (general rule is make it smaller than pitch)
[phantom_grid,w,z] = makePhantomGrid(w,z,dx);

%% Define target geometry
target_diameter = 6e-3;  % target diameter [m]
[target_map, target_case] = makeTarget(phantom_grid,'circle',dx,'Radius',target_diameter/2);

%% Make optical properties/absorber distribution
seed_number = rng;                               % seed number for random distribution generation
[mu_map, g_map, chi_map, target_map] = makeOpticalPhantom(target_map,'randomEverywhere',dx,seed_number,...
                                       'TargetCase',target_case,'PlotLayout',true);

%% Make fluence distribution
fiber_width = inf; %5e-3; %[m]
fiber_depth = round(size(target_map,1)/2) - 12e-3/2/dx;
[f_map] = makeFluence(target_map,fiber_width,dx,...
                      'FiberDepth',fiber_depth,...
                      'Attenuation',true,...
                      'PlotLayout',true);
                  
%% Make pressure map
snr = 25; %25;    % desired SNR of initial pressure distribution [dB] (note snr = nan means no noise is added)
[~,pMap_smooth,~,~] = makePressure(target_map, chi_map, mu_map, g_map, f_map,...
                                 snr, seed_number, dx,...
                                 'ZeroMean', true, 'PlotLayout', true);

%% Run theoretical SLSC algorithm
sprintf('Calculating SLSC')

% =========================================================================
% SIMULATION
% =========================================================================
maxM = 30;
tic
[slsc_theory] = makeSLSC(pMap_smooth, target_map, transducer,dx , target_case,...
                         'SimRange', 'image',...
                         'maxM',maxM);
toc

%% Visualize algorithm results -- SLSC images
Moi = [1,2,3,4,8,14]; % Lag values to plot

% Plot SLSC images at multiple lags
figure('Position',[66 59 1830 860]);
for i = 1:length(Moi)
    subplot(2,4,i);
    imagesc(slsc_theory.x_axis*1e3,slsc_theory.z_axis*1e3,...
            slsc_theory.slsc_avg(:,:,Moi(i))./max(max(slsc_theory.slsc_avg(:,:,Moi(i)))),[0 1]);
    colormap gray;
    axis image
    title(sprintf('M=%d',Moi(i)));
end

% Plot lateral SLSC profile at depth of target center at multpile lags 
L = cell(1,length(Moi));
subplot(224)
for i = 1:length(Moi)
    plot(slsc_theory.x_axis*1e3,slsc_theory.slsc_avg(round(size(phantom_grid,1)/2),:,Moi(i))./max(slsc_theory.slsc_avg(round(size(phantom_grid,1)/2),:,Moi(i))));
    hold on
    xlabel('Lateral (cm)');
    title('Lateral Profile');
    L(i) = {sprintf('M=%d',Moi(i))};
end
legend(L);

%% Visualize algorithm results -- coherence function

lag_per = (1:size(slsc_theory.cc_avg,2))./size(slsc_theory.cc_avg,2).*100; %lag axis [% of aperture]
lateral_position = 65; % lateral position of coherence function
M = 14;

figure('Position',[417 415 1180 422]);
subplot(121);
plot(lag_per,slsc_theory.cc_avg(lateral_position,:)./max(slsc_theory.cc_avg(lateral_position,:)));
xlabel('Lag (%)');ylabel('Coherence');
xlim([0 30]);ylim([-0.2 1]);

subplot(122);
plot(slsc_theory.x_axis*1e2,slsc_theory.slsc_avg(round(size(phantom_grid,1)/2),:,M)./max(slsc_theory.slsc_avg(round(size(phantom_grid,1)/2),:,M)));
xlabel('cm');ylabel('Cumulative Lag'); %Cumulative lag is synonymous with SLSC pixel intensity 
hold on
h = plot([lateral_position*slsc_theory.pitch*1e2, lateral_position*slsc_theory.pitch*1e2], [-5 5], '--k');
xlim([-inf, inf]);ylim([-0.3 1]);
legend(h,'Location of \newline Coherence Function');
